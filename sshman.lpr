program sshman;

{$mode objfpc}{$H+}

uses
	{$IFDEF UNIX}{$IFDEF UseCThreads}
	cthreads,
	{$ENDIF}{$ENDIF}
	Classes, SysUtils, CustApp, Process, Unix
	{ you can add units after this };

type

	{ TSSHMan }

  TProfile = record
    Name, Username, Address, Port: String[255];
	end;

  TDatabase = file of TProfile;

	TSSHMan = class(TCustomApplication)
	protected
		procedure DoRun; override;
	public
		constructor Create(TheOwner: TComponent); override;
		destructor Destroy; override;
		procedure WriteHelp; virtual;
  private
    Db: TDatabase;
    procedure AddRecord(NewRecord: TProfile);
    procedure DeleteRecord(RecordName: String);
    function GetRecord(RecordName: String) : TProfile;
	end;

{ TSSHMan }

const
  DbFile = '/home/mark/.config/sshman-laz.dat';

procedure TSSHMan.AddRecord(NewRecord: TProfile);
begin
  Seek(Db, FileSize(Db));
  Write(Db, NewRecord);
end;

procedure TSSHMan.DeleteRecord(RecordName: String);
var
  Profiles: array of TProfile;
  TempFile: TProfile;
  Index, Skipped: Integer;
begin
  SetLength(Profiles, FileSize(Db)); // Set dynamic array length to filesize
  Seek(Db, 0); // Seek back to beginning of database

  Index:= 0;
  Skipped:= 0;

  while not Eof(Db) do
  begin
    Read(Db, TempFile);
    if TempFile.Name = RecordName then begin
      Index:= Index - 1;
      Skipped:= Skipped + 1
		end
    else begin
      Profiles[Index]:= TempFile;
		end;
    Index:= Index + 1;
	end;

  SetLength(Profiles, FileSize(Db) - Skipped);
  Rewrite(Db);
  Seek(Db, 0);

  for TempFile in Profiles do
  begin
  	Write(Db, TempFile);
	end;
end;

function TSSHMan.GetRecord(RecordName: String): TProfile;
begin
  Seek(Db, 0);
  while not EOF(Db) do
	begin
  	Read(Db, GetRecord);
    if GetRecord.Name = RecordName then begin
      break
		end
    else begin
      GetRecord.Name:= '';
      GetRecord.Address:= '';
      GetRecord.Port:= '';
      GetRecord.Username:= '';
		end;
	end;
end;

procedure TSSHMan.DoRun;
var
	ErrorMsg: String;
  TempFile: TProfile;
  AProcess: TProcess;
begin
	// quick check parameters
	ErrorMsg:=CheckOptions('hngrlacix',['help', 'new', 'get', 'remove', 'list', 'address', 'connect', 'identify', 'xforward']);
	if ErrorMsg<>'' then begin
		ShowException(Exception.Create(ErrorMsg));
		Terminate;
		Exit;
	end;

	// parse parameters
	if HasOption('h','help') then begin
		WriteHelp;
		Terminate;
		Exit;
	end;

  AssignFile(Db, DbFile);

  if FileExists(DbFile) then begin
    Reset(Db)
  end
  else begin
    Rewrite(Db);
	end;

  if HasOption('n', 'new') then begin
	  Write('Name > ');
	  ReadLn(TempFile.Name);
	  Write('Username > ');
	  ReadLn(TempFile.Username);
	  Write('Address > ');
	  ReadLn(TempFile.Address);
	  Write('Port > ');
	  ReadLn(TempFile.Port);
    if TempFile.Port = '' then TempFile.Port:= '22';
	  AddRecord(TempFile);
    WriteLn('Profile ', TempFile.Name, ' saved successfully!');
    Terminate;
    Exit;
	end;

  if HasOption('g', 'get') then begin
    TempFile:= GetRecord(GetOptionValue('g', 'get'));
    WriteLn(TempFile.Name, '= ', TempFile.Username, '@', TempFile.Address, ':', TempFile.Port);
    Terminate;
    Exit;
	end;

  if HasOption('r', 'remove') then begin
    TempFile:= GetRecord(GetOptionValue('r', 'remove'));
    if TempFile.Name = '' then begin
      WriteLn('ERROR: Profile does not exist. Exiting.')
		end
    else begin
    	DeleteRecord(GetOptionValue('r', 'remove'));
		end;
		Terminate;
    Exit;
	end;

  if HasOption('l', 'list') then begin
    Seek(Db, 0);
    while not Eof(Db) do
    begin
      Read(Db, TempFile);
      Write(TempFile.Name, ': ');
      WriteLn(#9, TempFile.Username, '@', TempFile.Address, ':', TempFile.Port);
		end;
    Terminate;
    Exit;
	end;

  if HasOption('a', 'address') then begin
    WriteLn(GetRecord(GetOptionValue('a', 'address')).Address);
    Terminate;
    Exit;
	end;

  if HasOption('c', 'connect') then begin
    TempFile:= GetRecord(GetOptionValue('c', 'connect'));
    if TempFile.Name = '' then begin
      WriteLn('ERROR: Profile does not exist. Exiting.')
		end
    else begin
			AProcess:= TProcess.Create(nil);
    	AProcess.Executable:= '/usr/bin/ssh';
    	AProcess.Parameters.Add('-p');
    	AProcess.Parameters.Add(TempFile.Port);
    	AProcess.Parameters.Add('-tt');
    	AProcess.Parameters.Add(TempFile.Username + '@' + TempFile.Address);
    	AProcess.Options:= [];
    	AProcess.ShowWindow:= swoShow;
    	AProcess.InheritHandles:= False;
    	AProcess.Execute;
    	AProcess.WaitOnExit;
    	AProcess.Free;
		end;
		Terminate;
    Exit;
	end;

  if HasOption('x', 'xforward') then begin
  	TempFile:= GetRecord(GetOptionValue('x', 'xforward'));
    if TempFile.Name = '' then begin
      WriteLn('ERROR: Profile does not exist. Exiting.')
		end
    else begin
			AProcess:= TProcess.Create(nil);
    	AProcess.Executable:= '/usr/bin/ssh';
      AProcess.Parameters.Add('-X');
    	AProcess.Parameters.Add('-p');
    	AProcess.Parameters.Add(TempFile.Port);
    	AProcess.Parameters.Add('-tt');
    	AProcess.Parameters.Add(TempFile.Username + '@' + TempFile.Address);
    	AProcess.Options:= [];
    	AProcess.ShowWindow:= swoShow;
    	AProcess.InheritHandles:= False;
    	AProcess.Execute;
    	AProcess.WaitOnExit;
    	AProcess.Free;
		end;
		Terminate;
    Exit;
	end;

  if HasOption('i', 'identify') then begin
    TempFile:= GetRecord(GetOptionValue('i', 'identify'));
    SysUtils.ExecuteProcess('/usr/bin/ssh-copy-id', ' -p ' + TempFile.Port + ' ' + TempFile.Username + '@' + TempFile.Address, []);
	end;

	// stop program loop
	Terminate;
end;

constructor TSSHMan.Create(TheOwner: TComponent);
begin
	inherited Create(TheOwner);
	StopOnException:=True;
end;

destructor TSSHMan.Destroy;
begin
	inherited Destroy;
end;

procedure TSSHMan.WriteHelp;
begin
	{ add your help code here }
  // hngrlaci
	WriteLn('Usage: sshman -h');
  WriteLn('Usage: sshman -n (--new)'#13#10#9'Invokes the prompt for creating a new profile\n\tTODO: Parsing');
  WriteLn('Usage: sshman -g (--get) <profile name>'#13#10#9'Retrieves profile information by name, or most recent if none provided');
  WriteLn('Usage: sshman -r (--remove) <profile name>'#13#10#9'Removes profile by name');
  WriteLn('Usage: sshman -l (--list)'#13#10#9'Lists all available profiles');
  WriteLn('Usage: sshman -a (--address) <profile name>'#13#10#9'Retrieves address of profile by name');
  WriteLn('Usage: sshman -c (--connect) <profile name>'#13#10#9'Opens up secure shell connection to <profile name>');
  WriteLn('Usage: sshman -i (--identify) <profile name>'#13#10#9'Identifies your public key to server listed in <profile name>');
end;

var
	Application: TSSHMan;
begin
	Application:=TSSHMan.Create(nil);
	Application.Title:='SSHMan';
	Application.Run;
	Application.Free;
end.

