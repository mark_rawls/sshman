# SSHMan #

## SSH Profile Manager for a modern age ##

Never have to worry about remembering IP addresses or ports or typing out tedious domain names. SSHMan is a simple, no fuss application to store and configure your login to different servers. No password or other information is stored beyond address, username, and port number. This information is stored in a binary blob at ```~/.config/sshman-laz.dat```

```
#!bash

Usage: sshman
Usage: sshman -n (--new)
        Invokes the prompt for creating a new profile
Usage: sshman -g (--get) <profile name>
        Retrieves profile information by name, or most recent if none provided
Usage: sshman -r (--remove) <profile name>
        Removes profile by name
Usage: sshman -l (--list)
        Lists all available profiles
Usage: sshman -a (--address) <profile name>
        Retrieves address of profile by name
Usage: sshman -c (--connect) <profile name>
        Opens up secure shell connection to <profile name>
Usage: sshman -i (--identify) <profile name>
        Identifies your public key to server listed in <profile name>

```